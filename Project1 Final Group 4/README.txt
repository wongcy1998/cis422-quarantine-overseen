Real-time Geospatial Data Tracking System

Authors:
    Yuchen Gao
    Mounir El Jamal
    Joe Johnson
    Nolan Rudolph
    Toby Wong

The system was created and complete on the 27th of April, 2020.

The purpose of the system was to provide geospatial data tracking in aid of
social distancing during the pandemic of COVID-19.

There are no compiling requirements required.

The following softwares are required:
    MySQL version 5.7

File Descriptions
-------------------------------------------------------------
Root Directory/
	
	Documentation/
		programmer.txt:
			Gives overview of files as well as installation procedures

		user.pdf
			Gives information on how user can interact with system
			
		Project 1 Plan.pdf
			project 1 plan
			
		SRS
			System requirements
			
		SDS
			Software Design Specifications

	Code/
		README.txt (this file):
			Gives initial data about use and run procedure as well as directory information

		ajaxify.php
			Allows ajax tracking

		export_txt.php
			exports database as txt

		export_csv.php
			exports database as csv

		index.html
			web portal coding

		backup.sh
			backup script

		config.php
			configs
		
		my.cnf
			configure

		setup.sh
			setup script
		
		assets/
			config.php
				configure
		
			database.php
				setup database

			track.php
				tracks		

		style/
			bootstrap.css
				used for layout of web portal

		assets/
			track.php
				controls information tracked and sent

			database.php
				set up of database

			config.php
				informs track.php on how to connect