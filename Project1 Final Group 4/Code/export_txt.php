<?php
include_once "assets" . DIRECTORY_SEPARATOR . "config.php";
include_once "assets" . DIRECTORY_SEPARATOR . "database.php";

@header("Content-Disposition: attachment; filename=database_export.txt");

$database = new DB;
$handle = $database->connectToDb();
$query="SELECT * FROM rider_track";
$result = $handle->query($query);
$data = "";
$data .= "User I.D.\tDate\tTime\tLatitude\tLongitude\tTime at Location\n";
while($row=$result->fetch_array(MYSQLI_ASSOC))
{
$data.=$row['rider_id']."\t"; $data.=$row['track_time']."\t"; $data.=$row['track_lng']."\t"; $data.=$row['track_lat']."\t"; $data.=$row['track_duration']."\n";
}
echo $data;
exit();
?>
