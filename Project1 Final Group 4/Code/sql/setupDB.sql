SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE db;
USE db;

CREATE TABLE `rider_track` (
  `rider_id` varchar(120) NOT NULL,
  `track_time` varchar(20) NOT NULL DEFAULT 0,
  `track_lng` decimal(11,4) NOT NULL,
  `track_lat` decimal(11,4) NOT NULL,
  `track_duration` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

COMMIT;
