<?php
include_once "assets" . DIRECTORY_SEPARATOR . "config.php";
include_once "assets" . DIRECTORY_SEPARATOR . "database.php";

@header("Content-Disposition: attachment; filename=database_export.csv");

$database = new DB;
$handle = $database->connectToDb();
$query="SELECT * FROM rider_track";
$result = $handle->query($query);
$data = "";
$data .= "RIDER_ID, TRACK_TIME, TRACK_LNG, TRACK_LAT, TRACK_DURATION, \n";
while($row=$result->fetch_array(MYSQLI_ASSOC))
{
$data.=$row['rider_id'].",";
$data.=$row['track_time'].",";
$data.=$row['track_lng'].",";
$data.=$row['track_lat'].",";
$data.=$row['track_duration']."\n";
}
echo $data;
exit();
?>
