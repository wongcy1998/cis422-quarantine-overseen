<?php
// MUTE NOTICES
error_reporting(E_ALL & ~E_NOTICE);

// DATABASE SETTINGS -
define('DB_HOST', 'ix.cs.uoregon.edu');
define('DB_NAME', 'track_db');
define('DB_CHARSET', 'utf8');
define('DB_USER', 'guest');
define('DB_PASSWORD', 'guest');

// AUTO FILE PATH
define('PATH_LIB', __DIR__ . DIRECTORY_SEPARATOR);
?>