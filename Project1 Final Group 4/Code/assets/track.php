<?php
class RiderTrack
{
//FUNCTION TO LOG OR REGISTER THE NEW POSITION OF A USER/RIDER
public function logRider($riderid, $track_lng, $track_lat) {
  $curr_time = date("m/d/Y H:i:s");
  $database = new DB;
  //CONNECTING TO THE DB
  $handle = $database->connectToDb();
  //PREPARING QUERY
  $query = "INSERT INTO rider_track (rider_id, track_time, track_lng, track_lat) VALUES ('{$riderid}', '{$curr_time}', '{$track_lng}', '{$track_lat}')";
  if ($handle->query($query)) {
    return 1;
  }
  else {
    return 0;
  }
}
//WHEN THE SERVER DETECTS A NEW POSITION IT USES THE LAST LOGGED POSITION AND COMPARES IT WITH THE CURRENT POSITION
//IN CASE THE TWO POSITIONS MISMATCH THE USER HAS MOVED FROM ONE LOCATION TO ANOTHER
//IN SUCH A CASE THE PREVIOUS LOG IS CLOSED AND A TIME DURATION IS ADDED TO IT
//REPRESENTING FOR HOW LONG A USER HAS BEEN AT THAT LOCATION +-5 MINUTES
//FOR MORE ACCURACY CHANGE THE INTERVAL PARAMETER IN index.html
public function updateLog($riderid, $track_lng, $track_lat) {
  $curr_time = date("m/d/Y H:i:s");
  $database = new DB;
  //CONNECTING TO THE DB
  $handle = $database->connectToDb();
  //PREPARING QUERY
  $query = "SELECT * FROM rider_track WHERE rider_id = '{$riderid}' ORDER BY track_time DESC";
  $result = $handle->query($query);
  $rel_lng = strval(number_format((float)$track_lng, 4, '.', ''));
  $rel_lat = strval(number_format((float)$track_lat, 4, '.', ''));
  $myfile = fopen("debug.log", "w+");
  if ($result->num_rows > 0) {
    $row = $result->fetch_array(MYSQLI_ASSOC);
    $track_time = strtotime($row['track_time']);
    $net_duration_s = abs(strtotime($curr_time) - $track_time);
    $net_duration_m = ($net_duration_s - $net_duration_s % 60) / 60;
    $track_duration = $net_duration_m - $net_duration_m % 5;
    fwrite($myfile, "net_dur_s: ".strval($net_duration_s)."\nnet_dur_m: ".strval($net_duration_m)."\ntrack_dur: ".strval($track_duration));
    // User has not changed locations
    if (($rel_lng == $row['track_lng']) & ($rel_lat == $row['track_lat'])) {
      $query = "UPDATE rider_track SET track_duration = '{$track_duration}' WHERE rider_id = '{$riderid}' AND track_lng = '{$row['track_lng']}' AND track_lat = '{$row['track_lat']}' ORDER BY track_time DESC LIMIT 1";
      $handle->query($query);
      return 0;
    }
    // User has changed location
    else {
      $query = "UPDATE rider_track SET track_duration = '{$track_duration}' WHERE rider_id = '{$riderid}' ORDER BY track_time DESC limit 1";
      $handle->query($query);
      $this->logRider($riderid, $rel_lng, $rel_lat);
      return 0;
    }
  }
  else {
    $this->logRider($riderid, $rel_lng, $rel_lat);
    return 0;
  }
}
}
?>
