#!/bin/bash

##########################################################
###                                                    ###
### This script allows the user to setup a MySQL       ###
### on University of Oregon's ix-dev server.           ###
###                                                    ###
##########################################################

########## VALUES ##########

PORT='3712'
PASSWORD='guest'

########### CODE ###########

# Did the user supply enough arguments to this script?
if [ $# -ne 1 ]; then
  echo "Please use as \"$ bash $0 <user>\""
  echo "For example, if you are ngr@ix-dev.cs.uoregon.edu, do \"$ bash $0 ngr\"."
  exit 1
fi

USER=$1

# Ask if user really wants to follow through with running script
echo "This program will stop the services of any existing MySQL databases. If
/home/users/$USER/mysql-data exists, this will be moved to mysql-data-old in
the same path."
echo -n "Do you wish to proceed? [y / n] "
read userInput;
if [[ $userInput = 'n' ]] || [[ $userInput = 'N' ]]
then
	echo "Exiting...";
	exit 1;
elif ! [[ $userInput = 'y' ]] && ! [[ $userInput = 'Y' ]]
then
	echo "Please respond with y or n. Exiting...";
	exit 1;
fi

# Make sure script is ran from directory /path/to/ssd (probably ~/ssd)
if [[ -z `ls | grep "sql"` ]]; then
  if [[ -z `ls sql | grep "setupDB.sql"` ]]; then
    echo "Please restart script from the directory /path/to/ssd"
    exit 1
  fi
fi

# Confirm that "mysqlctl" is accessible from current directory
if [[ -z `echo $PATH | grep "/local/bin"` ]]; then
	export PATH=$PATH:/local/bin
fi

# Allow mysqlctl to run the "install" command
if [ -d "/home/users/$USER/mysql-data" ]; then
  mysqlctl stop > /dev/null;
  kill -9 `pidof mysql` > /dev/null
  kill -9 `pidof mysqld` > /dev/null
  kill -9 `pidof mysqlctl` > /dev/null
  mv /home/users/$USER/mysql-data /home/users/$USER/mysql-data-old
fi

# Re-install the server, and prompt the user to put a pre-defined password
printf "\n*** Please use password \"%s\" if prompted ***\n" "$PASSWORD"
mysqlctl install

# Use premade configuration file for MySQL Database, changing user to given arg
sed -i "s/meljamal/$USER/g" my.cnf
cp my.cnf ~/.my.cnf

# Start the new MySQL server
mysqlctl start

# Make sure it is running on port $PORT
if [[ -z `mysqlctl status | grep "3712"` ]]; then
  echo "[ERROR] MySql is not using port 3712."
  echo "We suggest running \"$ mysqlctl stop\", and \"$ /local/apps/mysql57/bin/mysqld stop\", waiting a minute, and then restarting this script."
  echo "If all else fails, use /local/apps/mysql57/bin/mysqladmin -p shutdown"
  exit 1
fi

# Load up schemas
mysql -p < sql/setupDB.sql

echo "Setup Complete! If no errors occurred, the DB is now functional."
