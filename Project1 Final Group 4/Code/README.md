# A Geospatial Data Collection System for Social Distancing
  
## Requirements
1. Access to the University of Oregon ix-dev server
2. mysqlctl

## Setup
1. $ git clone https://Nolan\_Rudolph@bitbucket.org/ssd422/ssd.git
2. $ cd ssd
3. $ bash setup.sh (Make sure to use password "guest")
  
## Files
**index.php**  
The interface a user is met with when connecting to our web application.

**config.php**  
Defines important variables to aid in establishing a connection with the MySQL
Database.

**Tracking/lib-track.php**  
Allows the user to extract all information from the MySQL Database to their
local machine.

**Tracking/ajax-track.php**  
Relays geospatial data acquired from the users spatial activity to the MySQL
Database every five minutes.

**setup.sh**  
Responsible for setting up the database required for storing geospatial data.

**backup.sh**  
Runs as a daemon process, periodically backing up data within the MySQL
Database.

**db.sql**  
Utilized by setup.sh to define schemas appropriate for storing geospatial data
in the MySQL Database.

**my.cnf**  
A premade MySQL Database configuration using ports and hosts that are
compatible with the rest of the repository.
